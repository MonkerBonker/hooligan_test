import os
import uuid

import cv2
from dotenv import load_dotenv

from src.utils.s3_client import YandexS3Client

if __name__ == '__main__':
    load_dotenv()
    s3_client = YandexS3Client(access_key=os.environ.get('AWS_ACCESS_KEY_ID'),
                               secret_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
                               )
    img_dir = "/home/art/Документы/HOOLIGAN/data/paris/1"
    # print(os.listdir(img_dir))
    image = cv2.imread(img_dir, cv2.IMREAD_COLOR)


    proj_dir =""
    tmp_dir = 'tmp/'
     #TODO auto define it with os
    tmp_image_path = tmp_dir
    os.makedirs(tmp_image_path, exist_ok=True)

    image_name = "{0}.jpg".format(str(uuid.uuid4()))  # TODO make img_hash
    image_path = os.path.join(tmp_image_path, image_name)
    cv2.imwrite(image_path ,image)

    s3_path = s3_client.save_image(image_path)
    os.remove(tmp_image_path)